<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transportes\Transporte;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class TransporteController extends Controller
{
    private $urlEndpointTransportes = "https://swapi.co/api/";
    /**
     * 
     * @return \Illuminate\Http\Response
     */
    public function calcular(Request $request) {
    
        $parametros = $request->all();

        $request->validate([
            'transporte' => 'required|string',
            'distancia' => 'required|integer'
        ]);

        $this->urlEndpointTransportes .= $parametros['transporte'];

        if (isset($parametros['pag'])) {
            $this->urlEndpointTransportes = $parametros['pag'];
        }

        $client = new Client();
        $resultado = $client->get($this->urlEndpointTransportes);

        $transportesEncontrados = "";
        if ($resultado->getStatusCode() == 200) {
            
            $dados = json_decode($resultado->getBody()->getContents());
            $transportesEncontrados = $dados->results;
        }  

        return view('index.index', [
            'transportes' => $transportesEncontrados,
            'tipo_transporte' => $parametros['transporte'],
            'distancia' => $parametros['distancia'],
            'next' => (isset($dados->next)) ? isset($dados->next) : null,
            'prev' => (isset($dados->prev)) ? isset($dados->prev) : null,
        ]);     
    }
}
