<?php

namespace App\Models\Transportes;

use Illuminate\Database\Eloquent\Model;

class Starship extends Model implements Interfaces\iTransporte
{
    private $dados = null;

    public function __construct($dadosDoTransporte)
    {
        $this->dados = $dadosDoTransporte;
    }

    /*
    * Determina a quantidade de paradas que uma nave estelar ira fazer de uma determinada distancia
    */
    public function paradas($distancia)
    {
    	try {

            if ($this->dados->consumables == 'unknown' || $this->dados->MGLT == 'unknown') {
                return 'Desconhecido';
            }

            $tempoEmMin = 0;
            $autonomia = explode(' ', $this->dados->consumables);
            $multiplicadorTempo = (int)$autonomia[0];
            $tempo = $autonomia[1];

            if ($tempo == 'day' || $tempo == 'days') {
                $tempoEmMin = 60 * 60 * 24;
            } else if ($tempo == 'week' || $tempo == 'weeks') {
                $tempoEmMin = 60 * 24 * 7;
            } else if ($tempo == 'month' || $tempo == 'months') {
                $tempoEmMin = 60 * 24 * 30;
            } else if ($tempo == 'year' || $tempo == 'years') {
                $tempoEmMin = 60 * 24 * 30 * 365;
            }
            
            $autonomiaEmMin = $multiplicadorTempo * $tempoEmMin;
            
            $tempoTotalGasto = (int)$distancia * 60 / (int)$this->dados->MGLT;
            return floor($tempoTotalGasto / $autonomiaEmMin);
    	} catch (\Exception $e) {
    		echo "Infelizmente houve uma falha no processamento.";exit;
    	}
    }
}
