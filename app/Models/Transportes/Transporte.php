<?php

namespace App\Models\Transportes;

/*
* Esta classe tem o objetivo de centralizar os comportamentos de qualquer tipo de transporte
*/
class Transporte
{
    private $transporte;

    public function __construct($tipoTransporte, $dadosDoTransporte)
    {
    	switch ($tipoTransporte) {
    		case 'starships':
    			$this->transporte = new Starship($dadosDoTransporte);
    		break;
    		/*
    		case 'Car':
    			$this->transporte = new Car();
    		break;
    		*/
    	}
    }

    public function paradas($distancia)
    {
    	return $this->transporte->paradas($distancia);
    }
}
