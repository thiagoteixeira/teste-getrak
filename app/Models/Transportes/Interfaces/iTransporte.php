<?php

namespace App\Models\Transportes\Interfaces;

interface iTransporte {
	public function __construct($dadosDoTransporte);
	public function paradas($distancia);
}