<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Pagina inicial
Route::get('/', 'IndexController@index');

// transportes
Route::post('/transporte/calcular/', 'TransporteController@calcular');
Route::get('/transporte/calcular/{w}/{a}/{d}', 'TransporteController@calcular');
