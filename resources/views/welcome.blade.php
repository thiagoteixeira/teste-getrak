<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    </head>
    <body>
        <h1>Teste - Getrak</h1>
        <br>
        <h3>Calcular a quantidade de paradas que um veículo irá realizar em uma determinada distância:</h3>
        <br>
        <form id='frmTransporte' name='frmTransporte' method="post" action="{{ url('transporte/calcular/') }}">
            {{ csrf_field() }}
            <label>Transporte: </label>
            <select id='transporteId' name='transporte'>
                <option>Selecione...</option>
                <option value="starship">Starship</option>
            </select>
            <label>Distância (MGLT): </label><input type="number" name="distancia" required="required" >
            <button name="btCalcular">Calcular</button>
        </form>
        @if (isset($transportes) && !is_null($transportes))
        <br>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            @if (count($transportes) > 0) 
            {{ dd($transportes) }}
                @foreach ($transportes as $transporte)
                    {{ 'O transporte '.$transporte->name. ' de modelo ' .$transporte->model. ' irá realizar ' .number_format($transporte->paradas, 0, ',','.'). ' parada(s) em uma distância de ' .number_format($distancia, 0, ',','.'). ' MGLT' }} <br><br>
                @endforeach
                <br>
                <a href='{{ $transportes->next }}'>Próximo</a>
            @else
                'Nenhum transporte foi encontrado';
            @endif
        @endif
    </body>
</html>
