@extends('principal')

@section('content')

<h1>Teste - Getrak</h1>
        <br>
        <h3>Calcular a quantidade de paradas que um veículo irá realizar em uma determinada distância:</h3>
        <br>
        <form id='frmTransporte' name='frmTransporte' method="post" action="{{ url('transporte/calcular/') }}">
            {{ csrf_field() }}
        <div class='row'>
            <div class='col-xs-2'>
            <label>Transporte: </label>
            <select class='form-control' id='transporteId' name='transporte'>
                <option>Selecione...</option>
                <option value="starships">Starship</option>
            </select>
            </div>
            <div class='col-xs-2'>
            <label>Distância (MGLT): </label><input class="form-control" type="number" name="distancia" required="required" >
            </div>
            <div class='col-xs-2'>
                <button class="form-control btn btn-primary" name="btCalcular">Calcular</button>
            </div>
        </div>
        </form>
        <br>
        @if (isset($transportes) && !is_null($transportes))
        <br>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            @if (count($transportes) > 0) 
            <table class='table table-bordered'>
                <tr>
                    <td>Nave</td>
                    <td>Modelo</td>
                    <td>Autonomia</td>
                    <td>MGLT</td>
                    <td>Qtd Paradas</td>
                </tr>
                @foreach ($transportes as $transporte)
                <tr>
                    <td>{{ $transporte->name }}</td>
                    <td>{{ $transporte->model }}</td>
                    <td>{{ $transporte->consumables }}</td>
                    <td>{{ $transporte->MGLT }}</td>
                    <td>
                    <?php    
                    $nave = new App\Models\Transportes\Transporte($tipo_transporte, $transporte);
                    echo $nave->paradas($distancia);
                    ?>
                    </td>
                </tr>
                @endforeach               
            </table>

                @if (isset($prev) && !is_null($prev))
                <a class='btn btn-primary' href="{{ url('transporte/calcular/'.$tipo_transporte.'/'.$distancia.'/'.$prev) }}">Anterior</a>
                @endif

                @if (isset($next) && !is_null($next))
                <a class='btn btn-primary' href="{{ url('transporte/calcular/'.$tipo_transporte.'/'.$distancia.'/'.$next) }}">Próximo</a>
                @endif

            @else
                'Nenhum transporte foi encontrado';
            @endif
        @endif

@endsection