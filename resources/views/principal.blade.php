<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="_token" content="{!! csrf_token() !!}" />
		
		<link rel="stylesheet" href="{{ URL::asset('node_modules/bootstrap/dist/css/bootstrap.min.css') }}" />
		@yield('estilos-especificos-css')
		<link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
	
        <title>Teste - Getrak</title>		
    </head>
    
    <body>
		<div class="container-fluid">
			@yield('content')
		</div>
    </body>
    
    <!-- javascript loading -->
    <script type="text/javascript" src="{{ URL::asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    @yield('bibliotecas-especificas-js')
    
    <script type="text/javascript">
		var baseUrl = {!! json_encode(url('/')) !!}
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
    
    
</html>
	